# How to

- Clone this repository;
- Start the docksal project with `fin project start`
- Put your PHP application at the folder `docroot`
- - Alternatively, remove the `docroot` folder and create a symbolic link to it using `ln -s /path/to/your/php/application docroot`
- Navigate to [http://php71.docksal/]
